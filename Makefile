CC=gcc
CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG

MKDIR_CMD=mcdir -p
RM_CMD=rm -rf

BUILD_DIR=build
SRC_DIR=src
OBJ_DIR=obj

FILES = util mem mem_debug test main

.PHONY: all clean test

all: $(addprefix $(BUILD_DIR)/, $(addsuffix .o, $(FILES))) $(BUILD_DIR)
	$(CC) -o $(BUILD_DIR)/main $(filter %.o, $^)

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c $(BUILD_DIR)
	$(CC) -c $(CFLAGS) $< -o $@

build:
	$(MKDIR_CMD) $(BUILD_DIR)


clean:
	$(RM) $(BUILD_DIR)


test:
	@+cd tester; make CC=$(CC)
