#define _DEFAULT_SOURCE
#include "test.h"

#include <inttypes.h>
#include <stdio.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

static void* heap;

static struct block_header* start_block;


void init_heap(){
    heap = heap_init(10000);
    if (heap == NULL) {
        err ("err: error in heap initialization\n");
    } else {
        debug_heap (stderr, heap);
    }
    start_block = (struct block_header*) heap;
}

/**
 * Обычное успешное выделение памяти.
 */
void test1() {
    debug("--- Start test 1 ---\n");
    void* data = _malloc(1000);
    if (data == NULL) {
        err ("err: test 1 failed\n");
    }
    debug_heap (stderr, heap);
    if (start_block->capacity.bytes != 1000 || start_block->is_free) {
        err ("err: test 1 failed\n");
    }
    debug ("test passed\n");
}

/**
 * Освобождение одного блока из нескольких выделенных.
 */
void test2() {
    debug("--- Start test 2---\n");
    void* data = _malloc(1000);
    _malloc(1000);
    _free(data);
    debug_heap(stderr, heap);
    if (!((struct block_header*) (((uint8_t*) data)-offsetof(struct block_header, contents)))->is_free) {
        err("err: test 2 failed\n");
    }
    debug ("test passed");
}

/**
 * Освобождение двух блоков из нескольких выделенных.
 */
void test3() {
    debug("--- Start test 3 ---\n");
    void* data1 = _malloc(1000);
    void* data2 = _malloc(1000);
    _free(data1);
    _free(data2);
    debug_heap(stderr, heap);
    if (!((struct block_header*) (((uint8_t*) data1)-offsetof(struct block_header, contents)))->is_free ||
            !((struct block_header*) (((uint8_t*) data2)-offsetof(struct block_header, contents)))->is_free) {
        err("err: test 3 failed\n");
    }
    debug ("test passed\n");
}

static void free_heap() {
    struct block_header* cur = start_block;
    while (cur != NULL) {
        if (!cur->is_free) {
            _free((void*) (((uint8_t*) cur) + offsetof(struct block_header, contents)));
            cur = cur->next;
        }
    }
}

/**
 * Память закончилась, новый регион памяти расширяет старый.
 */
void test4() {
    debug ("--- Start test 4 ---\n");

    free_heap();

    void* data = _malloc(20000);

    debug_heap(stderr, heap);

    if (((struct block_header*) (((uint8_t*) data)-offsetof(struct block_header, contents))) != heap ||
            ((struct block_header*) (((uint8_t*) data)-offsetof(struct block_header, contents)))->is_free ||
            ((struct block_header*) (((uint8_t*) data)-offsetof(struct block_header, contents)))->capacity.bytes != 20000) {
        err ("err: test 4 failed\n");
    }
    debug("test passed\n");
}

struct block_header* last_block() {
    for (struct block_header* last = start_block; ; last = last->next) {
        if (last->next == NULL) {
            return last;
        }
    }
}

/**
 * Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
 */
void test5() {
    debug ("--- Start test 5---\n");

    struct block_header* last = last_block();

    void* mem_addr = last + size_from_capacity(last->capacity).bytes;

    void* test5_taken_mem = mmap( (uint8_t*) (getpagesize() * ((size_t) mem_addr / getpagesize()
            + (((size_t) mem_addr % getpagesize()) > 0))),1000, PROT_READ | PROT_WRITE,
            MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);

    debug("test taken mem addr: %p\n", test5_taken_mem);

    void* data = _malloc(40000);

    debug_heap(stderr, heap);

    if (data == start_block->next || data == test5_taken_mem ||
        ((struct block_header*) (((uint8_t*) data)-offsetof(struct block_header, contents)))->capacity.bytes != 40000 ||
                ((struct block_header*) (((uint8_t*) data)-offsetof(struct block_header, contents)))->is_free) {
        err("err: test 5 failed\n");
    }
    debug ("test passed\n");
}

void test() {
    init_heap();
    test1();
    test2();
    test3();
    test4();
    test5();
    printf("All test passed");
}
